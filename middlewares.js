const DBConnector = require("./data/database");

const checkReqBody = (req, res, next) => {
  const { name, reduction } = req.body;

  if (name && reduction) {
    next();
  } else {
    res.send("Voucher n'a pas été ajouté dans database");
  }
};


module.exports = { checkReqBody};
