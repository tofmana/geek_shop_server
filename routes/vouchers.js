const router = require("express").Router();
const {
  getVouchers,
  addVoucher,
  updateVoucher,
  deleteVoucher,
} = require("../models/Vouchers");
const { checkReqBody} = require("../middlewares");

router.get("/vouchers", async (req, res) => {
  try {
    const [vouchersProducts] = await getVouchers();
    res.json(vouchersProducts);
  } catch (error) {
    console.log(error);
  }
});

router.post("/vouchers", checkReqBody, async (req, res) => {
  try {
    const [voucher] = await addVoucher(req.body.name, +req.body.reduction);
    res.send("bien ajouté dans la base de donées");
  } catch (error) {
    console.log(error);
  }
});

router.patch("/vouchers", checkReqBody, async (req, res) => {
  try {
    const [updatedVoucher] = await updateVoucher(
      +req.body.reduction,
      req.body.name
    );

    if (updatedVoucher.changedRows === 0) {
      res.send("Aucun voucher trouvé à modifier");
      console.log("AUCUN!!!!");
    } else {
      res.send("Voucher a été bien modifié dans la base de donées");
    }
  } catch (error) {
    console.log(error);
  }
});

router.delete("/vouchers", async (req, res) => {
  try {
    const [deletedVoucher] = await deleteVoucher(req.body.name);
    if (deletedVoucher.affectedRows === 0) {
      res.send("Voucher introuvable, alors n'a pas été supprimé");
    } else {
      res.send("Voucher a été bien supprimé");
    }
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
