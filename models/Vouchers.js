const DBConnector = require("../data/database");

const getVouchers = () => {
  return DBConnector.query("SELECT name, reduction FROM geek_shop.vouchers");
};

const addVoucher = (name, reduction) => {
  return DBConnector.query(
    `INSERT INTO geek_shop.vouchers (name, reduction) VALUES (?, ?)`,
    [name, reduction]
  );
};

const updateVoucher = (reduction, name) => {
  return DBConnector.query(
    "UPDATE geek_shop.vouchers SET reduction = ? WHERE name = ?",
    [reduction, name]
  );
};

const deleteVoucher = (name) => {
  return DBConnector.query('DELETE FROM geek_shop.vouchers WHERE name = ?', [name]);
}

module.exports = { getVouchers, addVoucher, updateVoucher, deleteVoucher };
