const router = require("express").Router();
const { getProducts } = require("../models/Products");


router.get("/products", async (req, res) => {
  try {
    const products = await getProducts();
    res.json(products[0]);
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
