require("dotenv").config();

const express = require("express");
const app = express();
const port = process.env.DB_PORT || 3000;

//Liste des fichier routes :
const home = require("./routes/home");
const products = require("./routes/products");
const vouchers = require("./routes/vouchers");
const cors = require("cors");

//Execute nos routes :
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use(home);
app.use(products);
app.use(vouchers);

//Ecoute sur le port 3000 puis lance notre server :
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
