const DBConnector = require('../data/database');

const getProducts = () => 
{
    return DBConnector.query('SELECT product_code, description, unit_price FROM geek_shop.products');
}

module.exports = {getProducts};